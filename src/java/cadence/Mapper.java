/**
 * cadence, a HBase row mapper / mini ORM
 * Copyright (c) 2012 Robert Roland.
 * Released under the terms of the MIT license. See LICENSE.
 */

package cadence;

import java.util.Map;

import clojure.lang.RT;
import clojure.lang.Symbol;
import clojure.lang.Var;

import org.apache.hadoop.hbase.client.Result;

/**
 * Java shim for dealing with the mapper.
 *
 * @author robert@robertroland.org
 * @since 2012-12-09
 */
public class Mapper {
    public static Map deserialize(Result row, Map mapperDefinition) {
        return (Map)DESERIALIZER.invoke(row, mapperDefinition);
    }

    // Clojure interface below

    private static final Var REQUIRE = RT.var("clojure.core", "require");

    static {
        REQUIRE.invoke(Symbol.intern("cadence.deserializers"));
    }

    private static final Var DESERIALIZER = RT.var("cadence.deserializers", "deserialize-row");
}
