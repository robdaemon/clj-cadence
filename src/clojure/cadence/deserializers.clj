(ns cadence.deserializers
  (:import [org.apache.hadoop.hbase.util Bytes]))

(defmulti deserialize-value
  "Deserialize a database value to the given type"
  (fn [row mapper-def column-def value-bytes]
    [(:type column-def)]))

(defn deserialize-column
  [mapper-def column-def row & [column-prefix]]
  "Deserialize an individual cell from the result"
  (let [colname (:name column-def)
        family-bytes (Bytes/toBytes (:family column-def))
        colname-bytes (if (nil? column-prefix)
                        (Bytes/toBytes colname)
                        (Bytes/toBytes (str column-prefix colname))) 
        family-map (. row getFamilyMap family-bytes)
        value (. family-map get colname-bytes)]
    {(keyword colname) 
     (do
       (printf "deserializing colname %s from family %s with value %s\n"
               (Bytes/toString colname-bytes)
               (Bytes/toString family-bytes)
               value)
       (deserialize-value row mapper-def column-def value))}))

(defn deserialize-row
  [row mapper-def]
  "Deserialize the result"
  (let [columns (:columns mapper-def)]
    (printf "dealing with row %s\n" row)
    (into {} (map (fn [column-def]
                    (deserialize-column mapper-def column-def row)) columns))))

(defn handle-hash
  [row mapper-def column-def & [vector-pos]]
  "Deserialize a hash from an HBase result. vector-pos is used for vectors of hashes"
  (let [column-prefix-base (str (:name column-def) (:delimiter mapper-def))
        column-prefix (if (nil? vector-pos)
                        column-prefix-base
                        (str column-prefix-base vector-pos (:delimiter mapper-def))) 
        delimiter (:delimiter mapper-def)
        members (:members column-def)]
    (into {} (map (fn [nested-column-def]
                    (deserialize-column mapper-def
                                        (assoc nested-column-def :family (:family column-def))
                                        row
                                        column-prefix))
                  members))))

(defn- simple-deserialize
  [method value-bytes]
  (if (nil? value-bytes)
    nil
    (method value-bytes)))

(defmethod deserialize-value [:String]
  [_ _ _ value-bytes]
  "Deserialize a string"
  (simple-deserialize #(Bytes/toString %) value-bytes))

(defmethod deserialize-value [:Long]
  [_ _ _ value-bytes]
  "Deserialize a Long (aka int64)"
  (simple-deserialize #(Bytes/toLong %) value-bytes))

(defmethod deserialize-value [:Integer]
  [_ _ _ value-bytes]
  "Deserialize an Integer (aka int32)"
  (simple-deserialize #(Bytes/toInt %) value-bytes))

(defmethod deserialize-value [:Double]
  [_ _ _ value-bytes]
  "Deserialize a Double"
  (simple-deserialize #(Bytes/toDouble %) value-bytes))

(defmethod deserialize-value [:Float]
  [_ _ _ value-bytes]
  "Deserialize a Float"
  (simple-deserialize #(Bytes/toFloat %) value-bytes))

(defmethod deserialize-value [:Short]
  [_ _ _ value-bytes]
  "Deserialize a Short"
  (simple-deserialize #(Bytes/toShort %) value-bytes))

(defmethod deserialize-value [:BigDecimal]
  [_ _ _ value-bytes]
  "Deserialize a BigDecimal"
  (simple-deserialize #(Bytes/toBigDecimal %) value-bytes))

(defmethod deserialize-value [:Boolean]
  [_ _ _ value-bytes]
  "Deserialize a Boolean"
  (simple-deserialize #(Bytes/toBoolean %) value-bytes))

(defmethod deserialize-value [:DateTime]
  [_ _ _ value-bytes]
  "Deserialize an int64 to a Java Date object"
  (simple-deserialize #(java.util.Date. (Bytes/toLong %)) value-bytes))

(defmethod deserialize-value [:VectorHash]
  [row mapper-def column-def value-bytes]
  "Deserialize a vector of hashes"
  (let [count-column-name (str (:name column-def) (:delimiter mapper-def) "count")
        family-map (. row getFamilyMap (Bytes/toBytes (:family column-def)))
        vector-size (Bytes/toLong (. family-map get (Bytes/toBytes count-column-name)))]
    (into [] (map (fn [vector-pos]
                    (handle-hash row mapper-def column-def vector-pos))
                  (range 0 vector-size)))))

(defmethod deserialize-value [:Hash]
  [row mapper-def column-def value-bytes]
  "Deserialize a hash"
  (handle-hash row mapper-def column-def))

(defmethod deserialize-value nil
  [_ _ _ _]
  "Deserialize a null value"
  nil)
