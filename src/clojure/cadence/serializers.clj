(ns cadence.serializers
  (:import [org.apache.hadoop.hbase.util Bytes]))

(defmulti serialize-value
  "Serialize a map value to the proper database value"
  (fn [a-map mapper-def column-def value-object]
    [(:type column-def)]))

(defn- simple-serialize
  [method value-object]
  (if (nil? value-object)
    nil
    (method value-object)))

(defn- to-bytes-serialize
  [value-object]
  "Use the simple Bytes/toBytes serializer"
  (simple-serialize #(Bytes/toBytes %) value-object))

(defmethod serialize-value [:String]
  [_ _ _ value-object]
  "Serialize a String"
  (to-bytes-serialize value-object))

(defmethod serialize-value [:Long]
  [_ _ _ value-object]
  "Serialize a Long"
  (to-bytes-serialize value-object))

(defmethod serialize-value [:Integer]
  [_ _ _ value-object]
  "Serialize an Integer"
  (to-bytes-serialize value-object))

(defmethod serialize-value [:Double]
  [_ _ _ value-object]
  "Serialize a Double"
  (to-bytes-serialize value-object))

(defmethod serialize-value [:Float]
  [_ _ _ value-object]
  "Serialize a Float"
  (to-bytes-serialize value-object))

(defmethod serialize-value [:Short]
  [_ _ _ value-object]
  "Serialize a Short"
  (to-bytes-serialize value-object))

(defmethod serialize-value [:BigDecimal]
  [_ _ _ value-object]
  "Serialize a BigDecimal"
  (to-bytes-serialize value-object))

(defmethod serialize-value [:Boolean]
  [_ _ _ value-object]
  "Serialize a Boolean"
  (to-bytes-serialize value-object))

(defmethod serialize-value [:DateTime]
  [_ _ _ value-object]
  "Serialize a DateTime as an int64"
  (to-bytes-serialize (. value-object getTime)))
