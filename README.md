[![Build Status](https://travis-ci.org/robertrolandorg/cadence.png)](https://travis-ci.org/robertrolandorg/cadence)

# cadence

A way to define an HBase schema and a simple mapping from a hash to / from rows within an HBase table.

## Usage

Still in progress. The existing tests are probably your best bet to figure out what's going on.

## Notes

This uses the Cloudera CDH3 version of HBase, only because this is the distribution of HBase I work with on a daily basis.
I will upgrade it to a newer HBase as soon as I need it.

## License

Copyright © 2012 Robert Roland

Distributed under the MIT License. See LICENSE.
