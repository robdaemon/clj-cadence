(ns cadence.deserializers-test
  (:import [org.apache.hadoop.hbase KeyValue]
           [org.apache.hadoop.hbase.client Result]
           [org.apache.hadoop.hbase.util Bytes])
  (:use clojure.test
        cadence.deserializers))

(def facebook-post
  {:table "facebook_post"
   :delimiter "|"
   :key {:parts [{:name "facebook_id" :type :SHA1}]}
   :columns [{:name "facebook_id" :type :String :family "post"}
             {:name "created_at" :type :DateTime :family "post"}
             {:name "title" :type :String :family "post"}
             {:name "text" :type :String :family "post"}
             {:name "link" :type :String :formatter "hyperlink" :family "post"}
             {:name "privacy" :type :String :family "post"}
             {:name "to" :type :VectorHash :family "post"
              :members [{:name "name" :type :String}
                        {:name "id" :type :String}
                        {:name "link" :type :String}]}
             {:name "user" :type :Hash :family "user"
              :members [{:name "name" :type :String}
                        {:name "id" :type :String}
                        {:name "link" :type :String :formatter "hyperlink"}]}
             {:name "counts" :type :Hash :family "counts"
              :members [{:name "shares" :type :Long}
                        {:name "comments" :type :Long}
                        {:name "likes" :type :Long}]}]})

(defn hbase-row []
  (let [row (Bytes/toBytes "a rowkey")
        post-family (Bytes/toBytes "post")]
    (Result. [(KeyValue. row
                         post-family
                         (Bytes/toBytes "facebook_id")
                         (Bytes/toBytes "1234_5678"))
              (KeyValue. row
                         post-family
                         (Bytes/toBytes "created_at")
                         (Bytes/toBytes (long 1353191589980)))
              (KeyValue. row
                         post-family
                         (Bytes/toBytes "title")
                         (Bytes/toBytes "a post title"))

              (KeyValue. row
                         post-family
                         (Bytes/toBytes "to|count")
                         (Bytes/toBytes 2))
              (KeyValue. row
                         post-family
                         (Bytes/toBytes "to|0|name")
                         (Bytes/toBytes "Rob"))
              (KeyValue. row
                         post-family
                         (Bytes/toBytes "to|0|id")
                         (Bytes/toBytes "12345"))
              (KeyValue. row
                         post-family
                         (Bytes/toBytes "to|0|link")
                         (Bytes/toBytes "http://www.example.org/12345"))
              (KeyValue. row
                         post-family
                         (Bytes/toBytes "to|1|name")
                         (Bytes/toBytes "Adam"))
              (KeyValue. row
                         post-family
                         (Bytes/toBytes "to|1|id")
                         (Bytes/toBytes "67890"))
              (KeyValue. row
                         post-family
                         (Bytes/toBytes "to|1|link")
                         (Bytes/toBytes "http://www.example.org/67890"))])))

(deftest deserialize-row-test
  (let [expected {:privacy nil,
                  :counts {:shares nil, :comments nil, :likes nil},
                  :created_at (java.util.Date. 1353191589980),
                  :user {:name nil, :id nil, :link nil},
                  :text nil,
                  :facebook_id "1234_5678",
                  :title "a post title",
                  :link nil,
                  :to [{:name "Rob" :id "12345" :link "http://www.example.org/12345"}
                       {:name "Adam" :id "67890" :link "http://www.example.org/67890"}]}]
    (testing "Checking to see if everything is included"
      (is (= expected (deserialize-row (hbase-row) facebook-post))))))

