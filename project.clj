(defproject cadence "0.1.0-SNAPSHOT"
  :description "A simple schema definition and hash mapper for HBase."
  :url "http://github.com/robertrolandorg/cadence"
  :license {:name "MIT License"
            :url "http://opensource.org/licenses/MIT"}

  :dependencies      [[org.clojure/clojure "1.5.0-RC14"]
                      [org.apache.hbase/hbase "0.92.2"]
                      [org.apache.hadoop/hadoop-common "2.0.2-alpha"]]

  :source-paths      ["src/clojure"]
  :java-source-paths ["src/java"]

  :profiles {:dev {:dependencies [[midje "1.5-alpha10"]
                                  [org.clojure/tools.nrepl "0.2.1"]]
                   :plugins [[lein-midje "2.0.1"]]}})
